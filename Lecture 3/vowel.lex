%{
#include	<stdlib.h>
#include	<stdio.h>
extern int	vowelCounter;
%}
%%
A	vowelCounter++;
E	vowelCounter++;
I	vowelCounter++;
O	vowelCounter++;
U	vowelCounter++;
a	vowelCounter++;
e	vowelCounter++;
i	vowelCounter++;
o	vowelCounter++;
u	vowelCounter++;
\n	{ }
.	{ }
%%
int	vowelCounter	= 0;

int	yywrap	()	{ return(1); }

int	main	()
{
  yylex();
  printf("%d\n",vowelCounter);
  return(EXIT_SUCCESS);
}
  
