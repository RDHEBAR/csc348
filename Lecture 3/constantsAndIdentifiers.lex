%{
  //  Compile with:
  //  $ flex -oex4_echoer.c ex4_echoer.lex
  //  $ gcc ex4_echoer.c -o ex4_echoer
%}
%%
\n			// ignore newline
("+"|-)?[0-9]+"."[0-9]*((e|E)("+"|-)?[0-9]+)?	{
			  float f = atof(yytext);
			  printf("Float: %g\n",f);
			}
("+"|-)?[0-9]*"."[0-9]+((e|E)("+"|-)?[0-9]+)?	{
			  float f = atof(yytext);
			  printf("Float: %g\n",f);
			}
("+"|-)?[0-9]+(e|E)("+"|-)?[0-9]+		{
			  float f = atof(yytext);
			  printf("Float: %g\n",f);
			}
("+"|-)?[0-9]+		{ int i = atoi(yytext); printf("Integer: %d\n",i); }
[A-Za-z_][A-Za-z_0-9]*	{ printf("Identifier: %s\n",yytext); }
\"[^\"]*\"		{ printf("String constant: %s\n",yytext); }
.			{ /* ignore other chars */ }
%%
int	yywrap	()	{ return(1); }

int	main	()
{
  yylex();
  return(0);
}
