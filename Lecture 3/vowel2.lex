%{
#include	<stdlib.h>
#include	<stdio.h>
extern int	vowelCounter;
%}
%%
(A|E|I|O|U|a|e|i|o|u)	vowelCounter++;
\n	{ }
.	{ }
%%
int	vowelCounter	= 0;

int	yywrap	()	{ return(1); }

int	main	()
{
  yylex();
  printf("%d\n",vowelCounter);
  return(EXIT_SUCCESS);
}
  
