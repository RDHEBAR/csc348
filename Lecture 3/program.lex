%{
%}

%%
\n				{ printf("\n"); }
(PROGRAM|Program|program)	{ printf("PROGRAM\n"); }
.				{ printf("Something else %c\n",yytext[0]); }

%%

int yywrap () { return(1); }
int main ()
{
  yylex();
  return(0);
}
