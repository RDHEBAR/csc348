%{
#include	<stdlib.h>
#include	<stdio.h>
extern int	charCounter;
extern int	endLineCounter;
%}
%%
\n	{ endLineCounter++; charCounter++; }
.	charCounter++;
%%
int	charCounter	= 0;
int	endLineCounter	= 0;

int	yywrap	()	{ return(1); }

int	main	()
{
  yylex();
  printf("%d\t%d\n",endLineCounter,charCounter);
  return(EXIT_SUCCESS);
}
  
