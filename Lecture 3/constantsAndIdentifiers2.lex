%{
  //  Compile with:
  //  $ flex -oex4_echoer.c ex4_echoer.lex
  //  $ gcc ex4_echoer.c -o ex4_echoer
#define QUOTE_CHAR	((char)34)
%}
%%
\n			// ignore newline
("+"|-)?[0-9]+"."[0-9]*((e|E)("+"|-)?[0-9]+)?	{
			  float f = atof(yytext);
			  printf("Float: %g\n",f);
			}
("+"|-)?[0-9]*"."[0-9]+((e|E)("+"|-)?[0-9]+)?	{
			  float f = atof(yytext);
			  printf("Float: %g\n",f);
			}
("+"|-)?[0-9]+(e|E)("+"|-)?[0-9]+		{
			  float f = atof(yytext);
			  printf("Float: %g\n",f);
			}
("+"|-)?[0-9]+		{ int i = atoi(yytext); printf("Integer: %d\n",i); }
[A-Za-z_][A-Za-z_0-9]*	{ printf("Identifier: %s\n",yytext); }
\"			{
			  printf("String constant: ");
			  int	c;

//			  for  ( c = input();
//				 (c >= 0) && (c != QUOTE_CHAR); 
//				 c = input()
//			       )

			  while  ( c = input(), (c >= 0) && (c != QUOTE_CHAR) )
			  {
			    if  (c == '\\')
			    {
			      c	= input();

			      switch  (c)
			      {
			      case 't' :
				putchar('\t');
				break;

			      case 'n' :
				putchar('\n');
				break;

			      case QUOTE_CHAR :
				putchar(QUOTE_CHAR);
				break;

			      case '\\' :
				putchar('\\');
				break;

			      case 'r' :
				putchar('\r');
				break;

			      default :
				return(-1);
			      }
			    }
			    else
			      putchar(c);
			  }
			  putchar('\n');
			}
"/*"			{
			  int c1 = 0; int c2 = input();
			  int	nesting	= 1;
			  while (1)
			  {
			    if (c2 <= 0)
			      break;
			    if (c1=='/' && c2=='*')
			      nesting++;
			    else
			    if (c1=='*' && c2=='/')
			    {
			      nesting--;

			      if  (nesting <= 0)
			        break;
			    }
			    c1 = c2; c2 = input();
			  }
			}
.			{ /* ignore other chars */ }
%%
int	yywrap	()	{ return(1); }

int	main	()
{
  if  (yylex() < 0)
  {
    fprintf(stderr,"Bad string ya big dummy!\n");
    return(EXIT_FAILURE);
  }
  return(0);
}
