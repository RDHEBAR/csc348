%{
  //  Compile with:
  //  $ flex ­-o ex2_echoer.c ex2_echoer.lex
  //  $ gcc ex4_echoer.c -­o ex2_echoer
%}
%%
quit	return(0);
\n	printf("\n");
.	printf("%c", yytext[0]);
%%
int     yywrap  ()      { return(1); }
int     main    ()
{
	printf("Type \"qiot\" to quit:\n");
  yylex();
  return(0);
}
// yytext[] array into which flex puts currently read lexeme