%{
  //  Compile with:
  //  $ flex ­-o ex4_echoer.c ex4_echoer.lex
  //  $ gcc ex4_echoer.c ­-o ex4_echoer
%}
%%
\n                      // Ignore newlines
("+"|­)?[0­9]+          { int i=atoi(yytext); printf("Integer: %d\n",i); }
[A­Za­z_][A­Za­z_0­9]*  { printf("Identifier: %s\n",yytext); }
.                       // Ignore other chars
%%
int     yywrap  ()      { return(1); }
int     main    ()
{
  yylex();
  return(0);
}
// yytext[] array into which flex puts currently read lexeme