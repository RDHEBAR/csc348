/*
 * pascal.lex: An example PASCAL scanner
 *
 */

%{
#include <stdio.h>

int line_number = 0;

void yyerror(char *message);

%}

%x COMMENT1 COMMENT2

white_space       [ \t]*
digit             [0-9]
alpha             [A-Za-z_]
alpha_num         ({alpha}|{digit})
hex_digit         [0-9A-F]
identifier        {alpha}{alpha_num}*
unsigned_integer  {digit}+
hex_integer       ${hex_digit}{hex_digit}*
exponent          e[+-]?{digit}+
i                 {unsigned_integer}
real              ({i}\.{i}?|{i}?\.{i}){exponent}?
string            \'([^'\n]|\'\')+\'
bad_string        \'([^'\n]|\'\')+

%%

"{"                  BEGIN(COMMENT1);
<COMMENT1>[^}\n]+
<COMMENT1>\n            ++line_number;
<COMMENT1><<EOF>>    yyerror("EOF in comment");
<COMMENT1>"}"        BEGIN(INITIAL);

"(*"                 BEGIN(COMMENT2);
<COMMENT2>[^)*\n]+
<COMMENT2>\n            ++line_number;
<COMMENT2><<EOF>>    yyerror("EOF in comment");
<COMMENT2>"*)"       BEGIN(INITIAL);
<COMMENT2>[*)]

 /* note that FILE and BEGIN are already 
  * defined in FLEX or C so they can't  
  * be used. This can be overcome in                               
  * a cleaner way by defining all the
  * tokens to start with TOK_ or some
  * other prefix.
  */

and                  return(0)
array                return(0)
begin                return(0)
case                 return(0)
const                return(0)
div                  return(0)
do                   return(0)
downto               return(0)
else                 return(0)
end                  return(0)
file                 return(0)
for                  return(0)
function             return(0)
goto                 return(0)
if                   return(0)
in                   return(0)
label                return(0)
mod                  return(0)
nil                  return(0)
not                  return(0)
of                   return(0)
packed               return(0)
procedure            return(0)
program              return(0)
record               return(0)
repeat               return(0)
set                  return(0)
then                 return(0)
to                   return(0)
type                 return(0)
until                return(0)
var                  return(0)
while                return(0)
with                 return(0)

"<="|"=<"            return(LEQ);
"=>"|">="            return(GEQ);
"<>"                 return(NEQ);
"="                  return(EQ);

".."                 return(DOUBLEDOT);

{unsigned_integer}   return(UNSIGNED_INTEGER);
{real}               return(REAL);
{hex_integer}        return(HEX_INTEGER);
{string}             return{STRING};
{bad_string}         yyerror("Unterminated string");

{identifier}         return(IDENTIFIER);

[*/+\-,^.;:()\[\]]   return(yytext[0]);

{white_space}        /* do nothing */
\n                   line_number += 1;
.                    yyerror("Illegal input");

%%

void yyerror(char *message)
{
   fprintf(stderr,"Error: \"%s\" in line %d. Token = %s\n",
           message,line_number,yytext);
   exit(1);
}